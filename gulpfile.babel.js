// import module
import gulp from 'gulp';
import babel from 'gulp-babel';
import gulpLoadPlugins from 'gulp-load-plugins';
import del from 'del';
import browserSync from 'browser-sync';

const fs = require('fs');
//const json = JSON.parse(fs.readFileSync('./package.json'));
// variable
const fontName  = 'pergi-Iconfonts';
const $         = gulpLoadPlugins();

// path
const paths = {
	bases: {
		src: 'src',
		dest: 'build'
	},
	views: {
		src: [
			'src/*.pug',
			'src/**/*.pug',
			'src/**/**/*.pug'],
		dest: 'build'
	},
	fonts: {
		src: 'src/assets/fonts/**/*',
		dest: 'build/assets/fonts'
	},
	images: {
		src: [
			'src/assets/images/**/*',
			'!src/assets/images/iconfonts/',
			'!src/assets/images/iconfonts/*'],
		dest: 'build/assets/images/',
		watch: 'src/assets/images/**/*'
	},
	styles: {
		src: [
			'src/assets/styles/main.scss',
			'src/assets/styles/layouts.scss',
			'src/assets/styles/newsletter.scss'	
		],
		dest: 'build/assets/styles',
		watch: 'src/assets/styles/**/*.scss'
	},
	scripts: {
		src: [
			'src/assets/scripts/components/*.js',
			'src/assets/scripts/layout.js',
			'src/assets/scripts/pages/**/*.js'],
		dest: 'build/assets/scripts/',
		watch: 'src/assets/scripts/**/*.js'
	}
};


/* write task start here */
/* task cleanup */
export const clean = () => del([ paths.bases.dest ]);

/* task views pug */
export const views = () => {
	global.firstRun = global.firstRun === undefined ? true : false;
	return gulp.src(paths.views.src)
		.pipe($.changed(paths.bases.dest, {extension: '.html'}))
		.pipe($.cached('pug-files'))
		.pipe($.if( !global.firstRun, $.pugInheritance({basedir: paths.bases.src, extension: '.pug', skip:'node_modules'}) ))
		.pipe($.plumber())
		.pipe($.pug({pretty: true}))
		.pipe(gulp.dest(paths.views.dest))
		.pipe(browserSync.stream());
};
/* task copy font */
export const fonts = () => {
  return gulp.src(paths.fonts.src)
    .pipe($.plumber())
    .pipe(gulp.dest(paths.fonts.dest))
};
/* task images */
export const images = () => {
	return gulp.src(paths.images.src, {since: gulp.lastRun(images)})
		.pipe($.plumber())
		.pipe($.newer(paths.images.dest))  /* pass through newer images only */
		.pipe($.imagemin({optimizationLevel: 5}))
		.pipe(gulp.dest(paths.images.dest));
};

/* task compile style */
export const styles = () => {
	return gulp.src(paths.styles.src)
		.pipe($.plumber())
		.pipe($.sass().on('error', $.sass.logError))
		.pipe($.changed(paths.styles.dest, {extension: '.css'}))
		.pipe($.cached('css-files'))
		.pipe($.cssmin())
		.pipe($.rename({suffix: '.min'}))
		.pipe($.sassUnicode())
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(browserSync.stream());
};

/* task concat js pages javascripts */
export const scripts = () => {
	return gulp.src(paths.scripts.src)
		.pipe($.plumber())
		.pipe(babel())
		.pipe($.concat('main.js'))
		.pipe(gulp.dest(paths.scripts.dest))
		.pipe(browserSync.stream());
};

/* task watchfile changed */
export const watch = () => {
	gulp.watch(paths.views.src, views);
	gulp.watch(paths.styles.watch, gulp.series(styles));
	gulp.watch(paths.scripts.watch, gulp.series(scripts));
	gulp.watch(paths.images.watch, gulp.series(images));
	//gulp.watch('src/assets/images/iconfonts/*.svg', gulp.series(iconfonts));
};
// task serve
const serve = () => browserSync.init({
	server: {
		baseDir: paths.bases.dest,
		serveStaticOptions: {
			extensions: ['html', 'js']
		}
	},
	notify: false
});

// task copy vendor
// export const vendor = (cb) => {
//   const obj = json.dependencies;
//   let arr = [];
//   Object.keys(obj).map(function(key) {
//     arr.push(key);
//   });

//   arr.map(function(res) {
//     return gulp.src(`node_modules/${res}/**/*`)
//       .pipe(gulp.dest(`build/assets/vendor/${res}`))
//       .on('end', cb);
//   })
// }

/*
 * You can still use `gulp.task`
 * for example to set task names that would otherwise be invalid
 */
const dev     = gulp.series(fonts, images, views, styles, scripts, gulp.parallel(serve, watch));
const build   = gulp.series(clean, fonts, images, views, styles, scripts );
gulp.task('build', build);
gulp.task('dev', dev);
/*
 * Export a default task
 */
export default dev;
