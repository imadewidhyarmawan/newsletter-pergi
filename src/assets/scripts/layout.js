
// Component Multilevel Sidebar Menu
  
  function changeContent() {
    // find content
    var pageContent = document.querySelectorAll('.lp-content');
    Array.prototype.forEach.call(pageContent, function(menupageContent, i){
      if (!hasClass(menupageContent, 'u-dn')) {
        var wrapperMenu = findSibling(menupageContent,'lp-sidebar');
        var linkMenu = wrapperMenu.querySelectorAll('.lp-sidebar__page');
        Array.prototype.forEach.call(linkMenu, function(link, i){
          if (!hasClass(link, 'u-dn')) {
            var linkAction = link.querySelectorAll('.lp-sidebar__page--menu');
            Array.prototype.forEach.call(linkAction, function(el, i){
              console.log(el);
              el.addEventListener('click', function(){
                var dataMenu = this.getAttribute('data-menu');
                var currentActive = findSiblingAll(this, 'is-active');
                console.log(currentActive);
                Array.prototype.forEach.call(currentActive, function(elm, i){
                  removeClass(elm, 'is-active');
                });
                console.log(el);
                addClass(this, 'is-active');
                var content = menupageContent.querySelectorAll('.lp-content__link');
                Array.prototype.forEach.call(content, function(el, i){
                  var contentDataMenu = el.getAttribute('data-menu');
                  if (contentDataMenu == dataMenu) {
                    removeClass(el, 'u-dn');
                  }
                  else {
                    addClass(el, 'u-dn');
                  }
                  var contentMobile = el.closest('.lp-content');
                  if(window.matchMedia('(max-width: 766px)').matches) {
                    addClass(contentMobile, 'is-show-mobile');
                    var back = document.querySelectorAll('.lp-content__back');
                    Array.prototype.forEach.call(back, function(el, i){
                      el.addEventListener('click', function() {
                        removeClass(contentMobile, 'is-show-mobile');
                      });
                    });
                  }
                });
              });
            });
          }
        });
      }
    });
  }

changeContent();
