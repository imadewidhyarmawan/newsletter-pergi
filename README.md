# Pergi E-mail Newsletter
Libraries and components website pergi.com
## Getting Started
### Clone Project
```
git clone ...
```
### Install
Make sure you have npm install and run
```
npm install
```
Install gulp-cli global
```
npm install gulp-cli -g
```
Install gulp
```
npm install gulp
```
### Run Project
Run before deployment (include clean up current build, without run server)
```
gulp build
```
Run server only / development
```
gulp
```


